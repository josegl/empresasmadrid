const fetch = require('isomorphic-fetch');
const cheerio = require('cheerio');

const results = {};

const wait = () => new Promise(resolve => {
  const secs = Math.floor(Math.random() * 20) + 1;
  setTimeout(resolve, secs * 1000);
});

const main = async () => {
  let list_page_link = 'https://www.einforma.com/informes-empresas/MADRID/Madrid-1.html';
  while(list_page_link) {
    await wait();
    const list_page_html = await fetch(list_page_link).then(response => response.text());
    const $ = cheerio.load(list_page_html);
    const page_links = $('a')
      .toArray()
      .filter(node => node.attribs && node.attribs.id)
      .filter(node=> /link-emp/.test(node.attribs.id))
      .map(node => node.attribs.href);
    for(const link of page_links){
      await wait();
      const enterpise_html = await fetch(link).then(response => response.text());
      const $1 = cheerio.load(enterpise_html);
      const CP = $1('th:contains("Código Postal")')[0].next.children[0].data;
      if(results[CP]){
        results[CP] = results[CP] + 1;
      } else {
        results[CP] = 1;
      }
    }
    const next_link_node = $('a:contains("siguiente")')
      .toArray()
      .find(node => node.attribs && node.attribs.href);
    list_page_link = next_link_node && next_link_node.attribs.href;
  }
  Object
    .entries(results)
    .forEach(([cp, number_of_enterpises]) => console.log(`${cp},${number_of_enterpises}`));
  return null;
}

main()
  .then(() => process.exit(0));
